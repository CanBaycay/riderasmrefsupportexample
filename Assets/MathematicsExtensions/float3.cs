﻿using static Unity.Mathematics.math;

namespace Unity.Mathematics
{

	public partial struct float3
	{
		public static readonly float3 one = new float3(1f, 1f, 1f);
	}

}
