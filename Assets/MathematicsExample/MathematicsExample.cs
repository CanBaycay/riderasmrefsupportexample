﻿using System;
using Unity.Mathematics;
using UnityEngine;

public class MathematicsExample : MonoBehaviour
{
	[NonSerialized]
	public float2 SomeFloat2_zero = float2.zero;
	[NonSerialized]
	public float3 SomeFloat3_zero = float3.zero;
	[NonSerialized]
	public float3 SomeFloat3_one = float3.one;

	private void Start()
	{
		Debug.Log("SomeFloat2_zero: \t" + SomeFloat2_zero);
		Debug.Log("SomeFloat3_zero: \t" + SomeFloat3_zero);
		Debug.Log("SomeFloat3_one: \t" + SomeFloat3_one);
	}
}
